package fr.test.services;

import fr.test.dao.Test;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Maxime on 14/03/2018.
 */
@Service
public interface TestService {

	List<Test> findAll();
}
