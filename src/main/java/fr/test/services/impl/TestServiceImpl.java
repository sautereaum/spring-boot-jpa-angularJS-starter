package fr.test.services.impl;

import fr.test.dao.Test;
import fr.test.repositories.TestRepository;
import fr.test.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Maxime on 14/03/2018.
 */
@Service
public class TestServiceImpl implements TestService {

	@Autowired
	private TestRepository testRepository;

	@Override
	public List<Test> findAll() {
		return testRepository.findAll();
	}
}
