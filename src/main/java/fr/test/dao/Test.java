package fr.test.dao;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Maxime on 13/03/2018.
 */
@Entity
public class Test {

	@Id
	long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
