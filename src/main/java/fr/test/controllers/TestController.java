package fr.test.controllers;

import fr.test.dao.Test;
import fr.test.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Maxime on 14/03/2018.
 */
@RestController
@RequestMapping("/api")
public class TestController {

	@Autowired
	private TestService testService;

	@RequestMapping("/test")
	public List<Test> findAll() {
		return testService.findAll();
	}

}
