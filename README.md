># _Starter for spring-boot-angularJS-starter_

## Installation with IntelliJ

1.  File .. New .. Project From Version Control .. Git
2.  Git : git@gitlab.etude.eisti.fr:sautereaum/spring-boot-angularJS-starter.git
3.  VCS Update project
4.  set db infos in application.properties
5.  Maven Project .. create configurations .. create 
    1. "clean install"
    2. "spring-boot:run"
6.  run new configurations -> remote
7.  start debug remote
7.  enjoy at localhost:8080
